package com.parraga.josue.evaluacion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Principal extends AppCompatActivity {


    EditText Cedula, Nombres, Apellidos, FechaNacimiento, Facultad;
    Button Ingreso, Consulta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        Cedula = (EditText) findViewById(R.id.txtCedula);
        Nombres = (EditText) findViewById(R.id.txtNombres);
        Apellidos = (EditText) findViewById(R.id.txtApellidos);
        FechaNacimiento = (EditText) findViewById(R.id.txtFechaNacimiento);
        Facultad = (EditText) findViewById(R.id.txtdepartamento);

        Ingreso = (Button) findViewById(R.id.btnIngresar);
        Consulta = (Button) findViewById(R.id.btnConsultar);

        final BaseDatos developeruBD = new BaseDatos(getApplicationContext());

        Ingreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseDatos.agregarCursos(Cedula.getText().toString(), Nombres.getText().toString(), Apellidos.getText().toString(), FechaNacimiento.getText().toString(), Facultad.getText().toString(),  Facultad.getText().toString());
                Toast.makeText(getApplicationContext(), "SE AGREGO CORRECTAMENTE", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
